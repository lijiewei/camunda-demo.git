# camunda-demo

## 介绍
springboot集成Camunda练习

## 问题
#### 1. 用mysql库，想让系统自动创建表，结果报错
```java
org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'org.camunda.bpm.spring.boot.starter.SpringBootProcessApplication$WebApplicationConfiguration': Unsatisfied dependency expressed through constructor parameter 0; nested exception is org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'org.camunda.bpm.spring.boot.starter.SpringBootProcessApplication': Unsatisfied dependency expressed through field 'processEngine'; nested exception is org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'processEngineFactoryBean': FactoryBean threw exception on object creation; nested exception is org.camunda.bpm.engine.ProcessEngineException: An exception occurred in the persistence layer. Please check the server logs for a detailed message and the entire exception stack trace.
```
原因：没有成功初始化脚本

解决：在https://downloads.camunda.cloud/release/camunda-bpm/下载camunda的zip包，提取出sql/create下脚本手动执行

初始化脚本只支持postgresql和cockroachdb自动初始化脚本

SchemaOperationsProcessEngineBuild 类判断建表策略

org.camunda.bpm.engine.impl.db.sql.DbSqlSession.isTablePresent类指明了只支持postgres和cockroachdb
```java
if (DatabaseUtil.checkDatabaseType(DbSqlSessionFactory.POSTGRES, DbSqlSessionFactory.CRDB)) {
    tableName = tableName.toLowerCase();
}
```

#### 2. 没有引入mybatis导致报错
```java
Exception encountered during context initialization - cancelling refresh attempt: org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'org.camunda.bpm.spring.boot.starter.SpringBootProcessApplication$WebApplicationConfiguration': Unsatisfied dependency expressed through constructor parameter 0; nested exception is org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'org.camunda.bpm.spring.boot.starter.SpringBootProcessApplication': Unsatisfied dependency expressed through field 'processEngine'; nested exception is org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'org.camunda.bpm.spring.boot.starter.CamundaBpmAutoConfiguration$ProcessEngineConfigurationImplDependingConfiguration': Unsatisfied dependency expressed through field 'processEngineConfigurationImpl'; nested exception is org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'processEngineConfigurationImpl' defined in class path resource [org/camunda/bpm/spring/boot/starter/CamundaBpmConfiguration.class]: Unsatisfied dependency expressed through method 'processEngineConfigurationImpl' parameter 0; nested exception is org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'camundaDatasourceConfiguration': Unsatisfied dependency expressed through field 'transactionManager'; nested exception is org.springframework.beans.factory.NoSuchBeanDefinitionException: No qualifying bean of type 'org.springframework.transaction.PlatformTransactionManager' available: expected at least 1 bean which qualifies as autowire candidate. Dependency annotations: {@org.springframework.beans.factory.annotation.Autowired(required=true)}
```
原因：camunda需要mybatis数据库，但是它自己并没有引入
